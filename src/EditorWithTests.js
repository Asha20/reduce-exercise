import CodeMirror from "../lib/codemirror";
import "../lib/mode/javascript";
import getTestResults from "./test";

function createEditor(textarea, className = "", config = {}) {
  const editor = CodeMirror.fromTextArea(textarea, {
    lineNumbers: true,
    mode: "javascript",
    lineWrapping: true,
    ...config
  });

  editor.display.wrapper.classList.add(className);
  return editor;
}

function inlineWidget(text, type, doc, position) {
  const element = document.createElement("pre");
  element.classList.add("widget", `color--${type}`);
  element.textContent = text;
  const widget = doc.addLineWidget(position, element, { above: true });
  return widget;
}

function gutterMarker(type, doc, position) {
  const element = document.createElement("div");
  const typeToClass = {
    success: "✔",
    fail: "✘",
    error: "❗"
  };
  element.textContent = typeToClass[type];
  element.classList.add("marker", `color--${type}`);
  doc.setGutterMarker(position, "status", element);
}

export default class EditorWithTests {
  constructor(config) {
    this._config = config;
    const { main, test } = config;
    this.mainEditor = createEditor(main.element, main.className, main.config);
    this.testEditor = createEditor(test.element, test.className, test.config);

    this.mainDoc = this.mainEditor.getDoc();
    this.testDoc = this.testEditor.getDoc();

    this.mainDoc.setValue(this.getContent("main", main.content, main.useCache));
    this.testDoc.setValue(this.getContent("test", test.content, test.useCache));
    this.mainEditor.refresh();
    this.testEditor.refresh();

    this.testWidgets = [];

    this.runTests = this.runTests.bind(this);

    this.mainEditor.setOption("extraKeys", {
      Tab: function(cm) {
        const spaces = Array(cm.getOption("indentUnit") + 1).join(" ");
        cm.replaceSelection(spaces);
      },
      "Ctrl-Enter": this.runTests,
      "Cmd-Enter": this.runTests
    });
  }

  getContent(editorName, content, useCache) {
    if (!useCache) {
      return content;
    }

    const cachedContent = localStorage.getItem(
      this._config.cacheKey + "/" + editorName
    );

    return cachedContent === null ? content : cachedContent;
  }

  saveMain() {
    localStorage.setItem(
      this._config.cacheKey + "/main",
      this.mainEditor.getDoc().getValue()
    );
  }

  saveTest() {
    localStorage.setItem(
      this._config.cacheKey + "/test",
      this.testEditor.getDoc().getValue()
    );
  }

  _renderTest({ line, message, type }) {
    const handle = this.testDoc.getLineHandle(line);
    if (type !== "pass") {
      this.testWidgets.push(inlineWidget(message, type, this.testDoc, handle));
      gutterMarker(type, this.testDoc, handle);
    }
  }

  runTests() {
    this.testDoc.clearGutter("status");
    while (this.testWidgets.length) {
      this.testWidgets.pop().clear();
    }

    const results = getTestResults(
      this.mainEditor.getDoc().getValue(),
      this.testEditor.getDoc().getValue()
    );

    results.tests.forEach(test => this._renderTest(test));
    return results;
  }
}
