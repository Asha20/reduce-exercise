import EditorWithTests from "./EditorWithTests";
import skeletonTemplate from "./templates/skeleton";
import testsTemplate from "./templates/tests";
import solutionsTemplate from "./templates/solutions";

const qs = (selector, parent = document) => parent.querySelector(selector);

const cm = new EditorWithTests({
  cacheKey: "implement-methods",
  main: {
    element: qs(".js-text-main-editor"),
    className: "editor--main",
    content: skeletonTemplate,
    useCache: true
  },
  test: {
    element: qs(".js-text-test-editor"),
    className: "editor--test",
    content: testsTemplate,
    useCache: true,
    config: {
      gutters: ["CodeMirror-linenumbers", "status"],
      readOnly: true
    }
  }
});

function renderResultCount({ success, fail, error }) {
  qs(
    ".js-results"
  ).textContent = `Successes: ${success} | Fails: ${fail} | Errors: ${error}`;
}

qs(".js-run").addEventListener("click", () => {
  try {
    const results = cm.runTests();
    renderResultCount(results.count);
  } catch (e) {
    qs(".js-results").textContent = "Error: " + e.message.split("\n")[0];
  }
});

qs(".js-reset").addEventListener("click", () => {
  cm.mainEditor
    .getDoc()
    .setValue(showingSolutions ? solutionsTemplate : skeletonTemplate);
});

const solutionsDoc = cm.mainDoc.copy();
solutionsDoc.setValue(solutionsTemplate);
solutionsDoc.cantEdit = true;
let showingSolutions = false;

cm.mainEditor.on("change", () => {
  if (!showingSolutions) {
    cm.saveMain();
  }
});

const btnShowSolutions = qs(".js-solutions");
btnShowSolutions.addEventListener("click", () => {
  showingSolutions = !showingSolutions;

  cm.mainEditor.swapDoc(showingSolutions ? solutionsDoc : cm.mainDoc);
  btnShowSolutions.textContent = showingSolutions
    ? "Go back to editing"
    : "View solutions";

  cm.mainEditor.focus();
});
