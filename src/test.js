import compare from "./compare";

function transpile(code) {
  return Babel.transform(code, {
    presets: ["es2015", "es2016", "es2017"],
    sourceType: "module"
  }).code;
}

// Since JSON doesn't accept undefined or NaN values,
// we stringify them manually.
function stringify(value) {
  if (Array.isArray(value)) {
    return "[" + value.map(stringify).join(", ") + "]";
  }

  if (value === undefined) {
    return "undefined";
  }
  if (value === null) {
    return "null";
  }
  if (Number.isNaN(value)) {
    return "NaN";
  }
  return JSON.stringify(value);
}

function stringifyTemplate(strings, ...objects) {
  let result = strings[0];

  const stringifiedObjects = objects.map(stringify);
  for (let i = 0; i < objects.length; i++) {
    result += stringifiedObjects[i] + strings[i + 1];
  }
  return result;
}

export function parseTests(testCode) {
  const tests = [];
  // Example match: test(every, [], x => x);
  // Captures: ["every", "[]", "x => x"]
  const testRegex = /^test\((\w+?), (\[.*?\])(?:, (.+?))?\);$/;
  testCode.split("\n").forEach((line, i) => {
    const match = line.match(testRegex);
    if (match !== null) {
      const [_, method, input, args] = match;

      const parsedInput = new Function(`return ${input}`)();
      const parsedArgs = new Function(`return [${args}]`)();
      tests.push({
        line: i,
        method,
        input: parsedInput,
        args: parsedArgs
      });
    }
  });

  return tests;
}

function runTest(fns, { line, method: methodName, input, args }) {
  try {
    if (!(methodName in fns)) {
      throw new Error(`Missing export "${methodName}"`);
    }

    const method = fns[methodName];
    const actual = method(input, ...args);
    const expected = input[method.name](...args);
    const passes = compare(actual, expected);

    if (!passes) {
      return {
        message: stringifyTemplate`Expected ${expected}; got ${actual}`,
        type: "fail",
        line
      };
    }
  } catch (err) {
    return {
      message: err.message,
      type: "error",
      line
    };
  }

  return {
    type: "success",
    line
  };
}

export default function runTests(userCode, testCode) {
  const code = new Function(`
    const exports = Object.create(null);

    (function userWrapper() {
      ${transpile(userCode)}
    })();

    return exports;
  `);

  const fns = code();
  const tests = parseTests(testCode);

  const results = tests.map(test => runTest(fns, test));
  const count = results.reduce(
    (acc, x) => {
      acc[x.type] += 1;
      return acc;
    },
    { success: 0, fail: 0, error: 0 }
  );

  return { tests: results, count };
}
