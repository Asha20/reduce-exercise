const tests = `
/*
The implementation of test looks something like this:

function test(method, input, ...args) {
  const actual = method(input, ...args);
  const expected = input[method.name](...args);

  return compare(actual, expected);
}
*/

test(every, [], x => x);
test(every, [1], x => x);
test(every, [0], x => x);
test(every, [1, 2, 3, 4, 5], x => x > 3);
test(every, [1, 2, 3, 4, 5], (x, i) => x + i > 5);
test(every, [8, 9, 3, 2, 1], (x, i, arr) => x < arr.length);

test(fill, [], 0);
test(fill, [1, 2, 3]);
test(fill, [0, 0, 0, 0, 0], 1);
test(fill, [0, 0, 0, 0, 0], 1, 2);
test(fill, [0, 0, 0, 0, 0], 1, 0, 3);
test(fill, [0, 0, 0, 0, 0], 1, 2, 4);

test(filter, [], x => x);
test(filter, [1], x => x);
test(filter, [0], x => x);
test(filter, [1, 2, 3, 4, 5], x => x > 3);
test(filter, [1, 2, 3, 4, 5], (x, i) => x + i > 5);
test(filter, [8, 9, 3, 2, 1], (x, i, arr) => x < arr.length);

test(find, [], x => x);
test(find, [1], x => x);
test(find, [0], x => x);
test(find, [1, 2, 3, 4, 5], x => x > 3);
test(find, [1, 2, 3, 4, 5], (x, i) => x + i > 5);
test(find, [5, 4, 3, 2, 1], (x, i, arr) => x < arr.length);

test(findIndex, [], x => x);
test(findIndex, [1], x => x);
test(findIndex, [0], x => x);
test(findIndex, [1, 2, 3, 4, 5], x => x > 3);
test(findIndex, [1, 2, 3, 4, 5], (x, i) => x + i > 5);
test(findIndex, [8, 9, 3, 2, 1], (x, i, arr) => x < arr.length);

test(includes, [], 0);
test(includes, [1, 2, undefined]);
test(includes, [0, 0, 0, 1, 0], 1);
test(includes, [0, 0, 0, 0, 0], 1, 2);
test(includes, [0, 0, 1, 0, 0], 1, 2);
test(includes, [0, 1, 0, 1, 0], 1, 2);

test(indexOf, [], 0);
test(indexOf, [0, 0, 1, 1, 0], 1);
test(indexOf, [0, 1, 0, 0, 0], 1, 2);
test(indexOf, [0, 1, 0, 0, 0], 1, 1);
test(indexOf, [0, 0, 0, 1, 0], 1, 2);

test(join, []);
test(join, [0]);
test(join, [1, 2, 3]);
test(join, [4, 5, 6], "|");

test(lastIndexOf, [], 0);
test(lastIndexOf, [0, 0, 1, 1, 0], 1);
test(lastIndexOf, [0, 0, 0, 1, 0], 1, 4);
test(lastIndexOf, [0, 0, 0, 1, 0], 1, 3);
test(lastIndexOf, [0, 0, 0, 1, 0], 1, 2);

test(map, [], x => x);
test(map, [1, 2, 3, 4, 5], x => x + 10);
test(map, [1, 2, 3, 4, 5], (x, i) => x * i);
test(map, [8, 9, 3, 2, 1], (x, i, arr) => x * arr[arr.length - i]);

test(reverse, []);
test(reverse, [1, 2, 3]);
test(reverse, [1, 2, 3, 5, 4, 6, 9]);

test(slice, []);
test(slice, [1, 2, 3]);
test(slice, [1, 2, 3, 4, 5], 2);
test(slice, [1, 2, 3, 4, 5], 1, 4);
test(slice, [1, 2, 3, 4, 5], 2, 4);

test(some, [], x => x);
test(some, [1], x => x);
test(some, [0], x => x);
test(some, [1, 2, 3, 4, 5], x => x === 9);
test(some, [1, 2, 3, 4, 5], (x, i) => x + i > 5);
test(some, [8, 9, 3, 2, 1], (x, i, arr) => x < arr.length);
`.trim();

export default tests;
