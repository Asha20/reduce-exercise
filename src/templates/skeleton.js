const skeleton = `
/*
Note that all of these methods are simplified
for the purpose of learning how to implement
them using reduce. In reality, native methods
are a bit more complex.

For convenience, for methods that take in a start
or end index such as fill, includes, indexOf
and such, you can safely assume that the index
will always be a non-negative integer.
*/

export function every(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function fill(array, value, start, end) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function filter(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function find(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function findIndex(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function includes(array, value, start) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function indexOf(array, value, start) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function join(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function lastIndexOf(array, value, start) {
  return array.reduceRight((acc, x, i) => {
    
  });
}

export function map(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

// Array.prototype.reduce actually mutates the array
// that it's called on; however, in this exercise,
// it's fine to write a version that creates a brand
// new array.
export function reverse(array, fn) {
  return array.reduceRight((acc, x, i) => {
    
  });
}

export function slice(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}

export function some(array, fn) {
  return array.reduce((acc, x, i) => {
    
  });
}
`.trim();

export default skeleton;
