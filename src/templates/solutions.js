const solutions = `
// Converts negative positions into positive ones.
function correctPosition(length, position) {
  return (position + length) % length;
}

function correctRange(length, start, end) {
  start = start === undefined
    ? 0
    : correctPosition(length, Math.trunc(start));
  end = end === undefined
    ? length
    : correctPosition(length, Math.trunc(end));
  
  // Convert any NaNs into zeroes.
  return {
    start: start || 0,
    end: end || 0,
  }
}

export function every(array, fn) {
  return array.reduce((acc, x, i) => {
    return Boolean(acc && fn(x, i, array));
  }, true);
}

export function fill(array, value, start, end) {
  ({start, end} = correctRange(array.length, start, end));

  return array.reduce((acc, x, i) => {
    if (i >= start && i < end) {
      acc.push(value);
    } else {
      acc.push(x);
    }
    return acc;
  }, []);
}

export function filter(array, fn) {
  return array.reduce((acc, x, i) => {
    if (fn(x, i, array)) {
      acc.push(x);
    }
    return acc;
  }, []);
}

export function find(array, fn) {
  return array.reduce((acc, x, i) => {
    if (acc !== undefined) {
      return acc;
    }

    return fn(x, i, array) ? x : undefined;
  }, undefined);
}

export function findIndex(array, fn) {
  return array.reduce((acc, x, i) => {
    if (acc !== -1) {
      return acc;
    }

    return fn(x, i, array) ? i : -1;
  }, -1);
}

export function includes(array, value, start) {
  ({start} = correctRange(array.length, start));

  return array.reduce((acc, x, i) => {
    if (acc || i < start) {
      return acc;
    }

    if (Number.isNaN(value) && Number.isNaN(value)) {
      return true;
    }

    return value === x;
  }, false);
}

export function indexOf(array, value, start) {
  ({start} = correctRange(array.length, start));

  return array.reduce((acc, x, i) => {
    if (acc !== -1 || i < start) {
      return acc;
    }

    return x === value ? i : -1;
  }, -1);
}

export function join(array, separator = ",") {
  return array.reduce((acc, x, i) => {
    if (i === array.length - 1) {
      return acc + x;
    }

    return acc + x + separator;
  }, "");
}

export function lastIndexOf(array, value, start) {
  // Since we're searching from the right, the end
  // of the range is actually the beginning.
  ({end: start} = correctRange(array.length, 0, start));

  return array.reduceRight((acc, x, i) => {
    if (acc !== -1 || i > start) {
      return acc;
    }

    return x === value ? i : -1;
  }, -1);
}

export function map(array, fn) {
  return array.reduce((acc, x, i) => {
    acc.push(fn(x, i, array));
    return acc;
  }, []);
}

export function reverse(array) {
  return array.reduceRight((acc, x) => {
    acc.push(x);
    return acc;
  }, []);
}

export function slice(array, start, end) {
  ({start, end} = correctRange(array.length, start, end));
  return array.reduce((acc, x, i) => {
    if (i >= start && i < end) {
      acc.push(x);
    }
    return acc;
  }, []);
}

export function some(array, fn) {
  return array.reduce((acc, x, i) => {
    return Boolean(acc || fn(x, i, array));
  }, false);
}
`.trim();

export default solutions;
