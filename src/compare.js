function compareArrays(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }
  return arr1.every((x, i) => compare(x, arr2[i]));
}

function compareObjects(x1, x2) {
  const keys1 = new Set(Object.keys(x1));
  const keys2 = new Set(Object.keys(x2));
  if (keys1.size !== keys2.size) {
    return false;
  }

  return [...keys1].every(key => compare(x1[key], x2[key]));
}

export default function compare(x1, x2) {
  if (Array.isArray(x1) && Array.isArray(x2)) {
    return compareArrays(x1, x2);
  }
  if (x1 === null || x2 === null) {
    return Object.is(x1, x2);
  }
  if (typeof x1 === "object" && typeof x2 === "object") {
    return compareObjects(x1, x2);
  }

  return Object.is(x1, x2);
}
