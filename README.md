# reduce-exercise

Implement various array methods using Array.prototype.reduce. If you would like to try doing so yourself, you can use [this link](https://asha20.gitlab.io/reduce-exercise).